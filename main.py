from datetime import date, datetime, time, timedelta
from pprint import pprint


def add_time(curr_time: time, delta: timedelta) -> time:
    return (datetime.combine(date(1, 1, 1), curr_time) + delta).time()


def sol(busy_slots: list[dict], size_window: int, start: str, end: str) -> list[dict]:
    ans = []
    start_day_at = time.fromisoformat(start)
    finish_day_at = time.fromisoformat(end)
    delta = timedelta(minutes=size_window)
    for i in range(len(busy_slots)):
        busy_slots[i] = {'start': time.fromisoformat(busy_slots[i]['start']),
                'stop': time.fromisoformat(busy_slots[i]['stop'])}
    busy_slots.sort(key=lambda x: x['start'])

    start_slot_at = start_day_at
    finish_slot_at = add_time(start_slot_at, delta)
    while finish_slot_at <= finish_day_at:
        if len(busy_slots):
            next_busy_slot_start, next_busy_slot_finish = busy_slots[0].values()
            if finish_slot_at > next_busy_slot_start:
                start_slot_at = next_busy_slot_finish
                finish_slot_at = add_time(start_slot_at, delta)
                busy_slots.pop(0)
                continue
        ans.append(
            {
                'start': start_slot_at.isoformat('minutes'),
                'stop': finish_slot_at.isoformat('minutes'),
            }
        )
        start_slot_at = finish_slot_at
        finish_slot_at = add_time(start_slot_at, delta)

    return ans


busy_slots = [
    {
        'start' : '10:30',
        'stop' : '10:50'
    },
    {   'start' : '18:40',
        'stop' : '18:50'
    },
    {
        'start' : '14:40',
        'stop' : '15:50'
    },
    {
        'start' : '16:40',
        'stop' : '17:20'
    },
    {
        'start' : '20:05',
        'stop' : '20:20'
    }
]


pprint(sol(busy_slots, 30, '09:00', '21:00'))
